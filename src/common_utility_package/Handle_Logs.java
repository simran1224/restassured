package common_utility_package;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Handle_Logs {

	public static File create_log_dir(String dirname) {
		String currentprojectdir = System.getProperty("user.dir");
		System.out.println(currentprojectdir);
		File log_directory = new File(currentprojectdir + "//API_Logs" + dirname);
		delete_dir(log_directory);
		log_directory.mkdir();
		return log_directory;
	}

	public static boolean delete_dir(File Directory) {
		boolean Directory_deleted = Directory.delete();
		if (Directory.exists()) {
			File[] files = Directory.listFiles();
			if (files != null) {
				for (File file : files) {
					if (file.isDirectory()) {
						delete_dir(file);
					} else {
						file.delete();
					}
				}
			}
			Directory_deleted = Directory.delete();
		}
		return Directory_deleted;
	}

	public static void evidence_creator(File dirname, String filename, String endpoint, String requestbody,
			String responsebody) throws IOException {

		// Step1 Create the file at given location

		File newfile = new File(dirname + "//" + filename + ".txt");
		System.out.println("new file created to save evidence :" + newfile.getName());

		// Step2 Write data into the file

		FileWriter datawriter = new FileWriter(newfile);
		datawriter.write("Endpoint :" + endpoint + "\n\n");
		datawriter.write("Requestbody:\n\n" + requestbody + "\n\n");
		datawriter.write("Responsebody:\n\n" + responsebody);
		datawriter.close();
		System.out.println("Evidence is written in files :" + newfile.getName());

	}

}
