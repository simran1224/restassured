package common_method_package;

import static io.restassured.RestAssured.given;

import request_repository.Put_Request_Repository;

public class Trigger_put extends Put_Request_Repository {
	
	public static int extract_status_code(String Requestbody, String URL) {
		int Statuscode = given().header("Content-Type","application/json").body(Requestbody).when().put(URL)
				.then().extract().statusCode();
		return Statuscode;
				}
	
	public static String Responsebody(String Requestbody, String URL) {
		String Responsebody = given().header("Content-Type","application/json").body(Requestbody).when().put(URL)
				.then().extract().response().asString();
		return Responsebody;
				}


}
