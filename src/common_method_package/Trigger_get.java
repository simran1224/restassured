package common_method_package;

import static io.restassured.RestAssured.given;

import request_repository.GET_Request_repository;


public class Trigger_get extends GET_Request_repository {
	
	public static int extract_status_code(String URL) {
		int Statuscode = given().when().post(URL)
				.then().extract().statusCode();
		return Statuscode;
				}
	
	public static String extract_Response_body(String URL) {
		String Responsebody = given().when().post(URL)
				.then().extract().response().asString();
		return Responsebody;
				}


}
