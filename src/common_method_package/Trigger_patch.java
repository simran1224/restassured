package common_method_package;

import static io.restassured.RestAssured.given;

import request_repository.Patch_Request_Repository;


public class Trigger_patch extends Patch_Request_Repository {
	
	public static int extract_status_code(String Requestbody, String URL) {
		int Statuscode = given().header("Content-Type","application/json").body(Requestbody).when().patch(URL)
				.then().extract().statusCode();
		return Statuscode;
				}
	
	public static String Responsebody(String Requestbody, String URL) {
		String Responsebody = given().header("Content-Type","application/json").body(Requestbody).when().patch(URL)
				.then().extract().response().asString();
		return Responsebody;
				}


}
