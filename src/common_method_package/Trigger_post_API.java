package common_method_package;

import static io.restassured.RestAssured.given;

import request_repository.Post_Request_Repository;

public class Trigger_post_API extends Post_Request_Repository {
	
	public static int extract_status_code(String requestbody, String URL) {
		int Statuscode = given().header("Content-Type","application/json").body(requestbody).when().post(URL)
				.then().extract().statusCode();
		return Statuscode;
				}
	
	public static String extract_Response_body(String requestbody, String URL) {
		String Responsebody = given().header("Content-Type","application/json").body(requestbody).when().post(URL)
				.then().extract().response().asString();
		return Responsebody;
				}

}
