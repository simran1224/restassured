import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import common_utility_package.Excel_data_reader;



public class Dynamic_Driver {

	public static void main(String[] args)
			throws IOException, ClassNotFoundException, NoSuchMethodException, SecurityException, InstantiationException
			,IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		// TODO Auto-generated method stub

		// Step1 read the test case to be executed from excel file

		ArrayList<String> TestCaseList = Excel_data_reader.Read_Excal_Data("Excal_data_reader.xlsx", "TestCasesToExecute",
				                         "TestCaseToExecute");
		System.out.println(TestCaseList);
		int count = TestCaseList.size();

		for (int i = 1; i < count; i++) {

			String TestCaseToExecte = TestCaseList.get(i);
			System.out.println("Test case which is going to be execute:"+ TestCaseToExecte);

			// Step2 Call the TestCaseToExecute on runtime by using java.lamg.reflect
			// package

			Class<?> TestClass = Class.forName("testclasspackage."+TestCaseToExecte);

			// Step3 Call the execute method of the class capture in variable

			Method ExecuteMethod = TestClass.getDeclaredMethod("executor");

			// Step4 set the accessiblity of the method as True

			ExecuteMethod.setAccessible(true);

			// Step5 Created the instance of class captured in the TestCase variable

			Object InstanceOfTestClass = TestClass.getDeclaredConstructor().newInstance();

			// Step6 Excute the method captured in the variable ExecuteMethod of class
			// capture in the TestClass variable

			ExecuteMethod.invoke(InstanceOfTestClass);

			System.out.println("Excution of Test Case Name " + TestCaseToExecte + " is completed ");
			System.out.println(":::::::::::::::::::::::::::");

		}

	}

}
