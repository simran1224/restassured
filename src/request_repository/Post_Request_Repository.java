package request_repository;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.ArrayList;

import common_utility_package.Excel_data_reader;


//request body and response body
public class Post_Request_Repository extends Endpoints {

	public static String post_TC1_Request() throws IOException {

		ArrayList<String> ExcelData = Excel_data_reader.Read_Excal_Data("Excal_data_reader.xlsx", "Post_API",
				"Post_TC2");
		System.out.println(ExcelData);
  
		String req_name = ExcelData.get(1);
		String req_job = ExcelData.get(2);

		String requestBody = "{\r\n" + "   " + " "
				            + "\"name\": \"" + req_name + "\",\r\n" + "   "
				            + " \"job\": \"" + req_job
				            + "\"\r\n" + "}";

		return requestBody;
	}
	// response body = url + requestbody

	public static String extract_Response_body(String requestBody, String URL) {
		String Responsebody = given().header("Content-Type", "application/json").body(requestBody).when().post(URL)
				.then().extract().response().asString();
		return Responsebody;
	}

}