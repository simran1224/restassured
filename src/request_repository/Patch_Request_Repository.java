package request_repository;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.ArrayList;


import common_utility_package.Excel_data_reader;

public class Patch_Request_Repository extends Endpoints {

	// request body and response body

	public static String Patch_TC1_Request() throws IOException {

		ArrayList<String> ExcelData = Excel_data_reader.Read_Excal_Data("Excal_data_reader.xlsx", "Patch_API",
				"Patch_TC2");
		System.out.println(ExcelData);

		String req_name = ExcelData.get(1);
		String req_job = ExcelData.get(2);

		String requestBody = "{\r\n"
				+ "    \"name\": \""+req_name+"\",\r\n"
				+ "    \"job\": \""+req_job+"\"\r\n"
				+ "}";

		return requestBody;
	}
	// response body = url + requestbody

	public static String extract_Response_body(String requestBody, String URL) {
		String Responsebody = given().header("Content-Type", "application/json").body(requestBody).when().patch(URL)
				.then().extract().response().asString();
		return Responsebody;
	}
}