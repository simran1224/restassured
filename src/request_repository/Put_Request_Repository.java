package request_repository;

import static io.restassured.RestAssured.given;

import java.io.IOException;
import java.util.ArrayList;

import common_utility_package.Excel_data_reader;



public class Put_Request_Repository extends Endpoints {
	public static String PUT_TC3_Requestbody() throws IOException {
		
	
	ArrayList<String> ExcelData = Excel_data_reader.Read_Excal_Data("Excal_data_reader.xlsx", "Put_API", "Put_TC2");
	System.out.println(ExcelData);
	
	String req_name = ExcelData.get(1);
	String req_job = ExcelData.get(2);
	
		String  Requestbody= "{\r\n"
				+ "    \"name\": \""+req_name+"\",\r\n"
				+ "    \"job\": \""+req_job+"\"\r\n"
				+ "}";
		
		return Requestbody;
	}
	
	public static String Responsebody(String Requestbody, String URL) {
		
		String Responsebody =given().header("Content-Type","application/json").body(Requestbody)
				             .when().log().all().put(URL).then().log().all().extract().response().asString();
		return Responsebody;
		
	}
}