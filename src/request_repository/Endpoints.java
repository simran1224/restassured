package request_repository;

public class Endpoints {
	
	// Endpoint of PostAPI


		// We are creating common URL for All the APIs
		// Static mean Without creating an object we are define Methods or Attributes
		static String hostname = "https://reqres.in/";

		public static String post_endpoint() {
			// We are inserting Source URL or Endpoint 
			String URL = hostname + "api/users";
			System.out.println(URL);
			return URL;
		}

		// Endpoint of PutAPI
		public static String Put_endpoint() {

			String URL = hostname + "api/users/2";
			System.out.println(URL);
			return URL;
		}

	// Endpoint of PatchAPI
		public static String Patch_endpoint() {

			String URL = hostname + "api/users/2";
			System.out.println(URL);
			return URL;

		}

	//Endpoint of GetAPI
		public static String Get_endpoint() {

			String URL = hostname + "api/users?page=2";
			System.out.println(URL);
			return URL;
		}
	}


