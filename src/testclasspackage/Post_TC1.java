package testclasspackage;

 import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import common_method_package.Trigger_post_API;

import common_utility_package.Handle_Logs;
import io.restassured.path.json.JsonPath;

public class Post_TC1 extends Trigger_post_API {
	@Test
	public static void executor() throws IOException  {
		
		File Dirname = Handle_Logs.create_log_dir("Post_TC1");
		int statuscode = 0;
	
		for (int i = 0; i < 5; i++) {
			
			int Statuscode = extract_status_code(post_TC1_Request(),Put_endpoint());
			System.out.println("status code is:" + Statuscode);

			if (Statuscode == 200) {
				String Responsebody = extract_Response_body(post_TC1_Request(),Put_endpoint());
				System.out.println("responsebody is:" + Responsebody);
				Handle_Logs.evidence_creator(Dirname, "Put_TC2",Put_endpoint(),post_TC1_Request(), Responsebody);
				validator(Responsebody);
				break;
			} else {
				System.out.println("status not found");
			}

		}

	}
	
	public static void validator(String Responsebody) throws IOException {
		
		
		JsonPath jsp_req = new JsonPath(post_TC1_Request());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		
		System.out.println(req_job);
		
		JsonPath jsp = new JsonPath(Responsebody);
	 String res_name = jsp.getString("name");
	 String res_job = jsp.getString("job"); 
	 String res_updatedAt = jsp.getString("updatedAt");
	 
	 String generatedDate = res_updatedAt.substring(0,10);
	 System.out.println(generatedDate);
	 
	 LocalDateTime CurrentDate = LocalDateTime.now();
	 
	 String newDate = CurrentDate.toString();
	 String updatedDate = newDate.substring(0, 10);
	 
	 System.out.println(updatedDate);

	 
	 Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(generatedDate, updatedDate);
		
		
	}

}
