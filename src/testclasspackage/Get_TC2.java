package testclasspackage;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import common_method_package.Trigger_get;
import common_utility_package.Handle_Logs;
import io.restassured.path.json.JsonPath;

public class Get_TC2 extends Trigger_get{
	
	@Test
	public static void executor() throws IOException {
		
		File DirName = Handle_Logs.create_log_dir("Get_TC1");
		
		for(int i=0;i<5;i++) {
		int Status_Code = extract_status_code(Get_endpoint());
		System.out.println("Status Code : " + Status_Code);
		
		if(Status_Code == 200) {
	    String ResponseBody = extract_Response_body(Get_endpoint());
		System.out.println("Response body : " + ResponseBody);
		
		Handle_Logs.evidence_creator(DirName, "Get_TC1", " ", Get_endpoint(), ResponseBody);
		
		validator(ResponseBody);
		break;
		
		}
		else {
			System.out.println("Desired Status code is invalid hence, retry");
		}
		
		}
		
	}
	
	public static void validator(String ResponseBody) {
		
		String[] Exp_id_Array = {"7","8","9","10","11","12"};
		
		String[] Exp_email_Array = {"michael.lawson@reqres.in","lindsay.ferguson@reqres.in","tobias.funke@reqres.in","byron.fields@reqres.in",
				"george.edwards@reqres.in","rachel.howell@reqres.in"};
		
		String[] Exp_FirstName_Array = {"Michael","Lindsay","Tobias","Byron","George","Rachel"};
		
		String[] Exp_LastName_Array = {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
		
		String[] Exp_Avatar_Array = {"https://reqres.in/img/faces/7-image.jpg","https://reqres.in/img/faces/8-image.jpg","https://reqres.in/img/faces/9-image.jpg",
				"https://reqres.in/img/faces/10-image.jpg","https://reqres.in/img/faces/11-image.jpg","https://reqres.in/img/faces/12-image.jpg"};

		
		
		JsonPath jsp_res = new JsonPath(ResponseBody);
		
		
		List<Object> res_data = jsp_res.getList("data");
		int count = res_data.size();
		
		for(int i=0;i<count;i++) {
			
			String Exp_id = Exp_id_Array[i];
			String Exp_email = Exp_email_Array[i];
			String Exp_First_Name = Exp_FirstName_Array[i];
			String Exp_Last_Name = Exp_LastName_Array[i];
			String Exp_Avatar = Exp_Avatar_Array[i];
			
			String res_id = jsp_res.getString("data["+i+"].id");
			
			String res_email = jsp_res.getString("data["+i+"].email");
			
			String res_first_name = jsp_res.getString("data["+i+"].first_name");
			
			String res_last_name = jsp_res.getString("data["+i+"].last_name");
			
			String res_avatar = jsp_res.getString("data["+i+"].avatar");
			
			
			//Validation 
			
			Assert.assertEquals(res_id, Exp_id);
			
			Assert.assertEquals(res_email, Exp_email);
			Assert.assertEquals(res_first_name, Exp_First_Name);
			Assert.assertEquals(res_last_name, Exp_Last_Name);
			Assert.assertEquals(res_avatar, Exp_Avatar);
		}
		
		
		
	}
	

}
